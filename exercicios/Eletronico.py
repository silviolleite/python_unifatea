class Eletronico:
  def __init__(self):
    self.ligado = False

  def ligar(self):
    self.ligado = True

  def desligar(self):
    self.ligado = False

  def esta_ligado(self):
    if(self.ligado):
      print("Ligado")
    else:
      print("Desligado")

class TV(Eletronico):
  def __init__(self):
    self.volume = 0
    Eletronico.__init__(self)

  def aumentar_volume(self):
    self.volume += 1

  def diminuir_volume(self):
    if self.volume > 0:
      self.volume -= 1

  def som(self):
    print("volume: {}".format(self.volume))


class Bluray(TV, Eletronico):
  def __init__(self):
    self.bandeja = "Fechado"
    Eletronico.__init__(self)
    TV.__init__(self)

  def abrir_bandeja(self):
    self.bandeja =  "Aberto"

  def fechar_bandeja(self):
    self.bandeja = "Fechado"

  def verificar_bandeja(self):
    print(self.bandeja)



    
